const axios = require("axios");

module.exports = class JSONPlaceholderController {

  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data;
      res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários captados da Api pública JSONPlaceholder",
          users,
        });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários" });
    }
  }
  static async getUsersWedsiteIO(req,res){

    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
        const users = response.data.filter(
          (user)=> user.website.endsWith(".io")
        )
        const Pessoas = users.length
        res
        .status(200)
        .json({
          message:
            "Quantas pessoas tem esse dominio:", Pessoas,
          users,
        });
    }catch(error){
  console.log(error)
  res.status(500).json({ error: "Deu ruim", error });

    }
  }

  static async getUsersWedsiteCOM(req,res){
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
        const users = response.data.filter(
          (user)=> user.website.endsWith(".com")
        )
        const Pessoas = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com dominio com. Quantas pessoas tem nesse dominio:", Pessoas,
          users,
        });
    }catch(error){
  console.log(error)
  res.status(500).json({ error: "Deu ruim", error });

    }
  }

  static async getUsersWedsiteNET(req,res){
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
        const users = response.data.filter(
          (user)=> user.website.endsWith(".net")
        )
        const Pessoas = users.length
        res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários com dominio net. Quantas pessoas tem nesse dominio:", Pessoas,
          users,
        });
    }catch(error){
  console.log(error)
  res.status(500).json({ error: "Deu ruim", error });

    }
  }

  static async getCountDomain (req, res) {
    const { dominio } = req.query;
    if(!dominio) {
      res.status(400).json({ message: `Informe um domínio válido`});
    }
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter((user)=> user.website && user.website.endsWith(dominio))

      res.status(200).json({ message:`Existem ${users.length} usuários com esse domínio`});

    }
    catch{
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários"});
    }
  
  }
};
