const router = require('express').Router()
const alunoController = require('../controller/alunoController')
const teacherController= require('../controller/teacherController')
const JSONPlaceholderController= require('../controller/JSONPlaceholderController')
router.get('/external/filter',JSONPlaceholderController.getCountDomain);

//ROTAS

router.get('/teacher/', teacherController.getTeacher),
router.post('/cadastroaluno/',alunoController.postAluno),
router.put('/updatealuno/',alunoController.postAluno),
router.delete('/deletealuno/:id',alunoController.deleteAluno),

router.get("/external/" ,JSONPlaceholderController.getUsers);
router.get("/external/io" ,JSONPlaceholderController.getUsersWebsiteIO);  
router.get("/external/com" ,JSONPlaceholderController.getUsersWebsiteCOM); 
router.get("/external/net" ,JSONPlaceholderController.getUsersWebsiteNET);


module.exports = router;